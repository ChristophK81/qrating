
function inArray(arr, value){
    var i;
    for (i=0; i < arr.length; i++) {
        if (arr[i] === value) {
            return true;
        }
    }
    return false;
};

function upgradeDatabaseTo1(event, db, tx){
        
        //console.log(new Date() + 'Start: upgradeDatabaseTo1');
        
        var classStore = db.createObjectStore('class', {keyPath: 'id', autoIncrement:true });
        classStore.createIndex('name_idx', 'name', {unique: true});
        classStore.createIndex('level_idx', 'level', {unique: false});
        classStore.put({id: 1, name: "Fuchs", level: 1}) ;
        classStore.put({id: 2, name: "Dachs", level: 2}) ;
        
        var userStore = db.createObjectStore('user', {keyPath: 'id', autoIncrement:true});
        userStore.createIndex('name_idx', 'name', {unique: true});
        userStore.put({id: 1, name: "Erna Meier" }) ;
        userStore.put({id: 2, name: "Max Mustermann", admin: 1}) ;
        userStore.put({id: 3, name: "Max Jems", admin: 0}) ;
        
        var moduleStore = db.createObjectStore('module', {keyPath: 'id', autoIncrement:true});
        moduleStore.createIndex('name_idx', 'name', {unique: false});
        moduleStore.createIndex('parentId_idx', 'parentId', {unique: false});
        moduleStore.createIndex('classId_idx', 'classId', {unique: false});
        moduleStore.put({id: 1, parentId: 0, classId: 1, name: "Mathe", description: "First Level Math"}) ;
        moduleStore.put({id: 2, parentId: 0, classId: 1, name: "Deutsch", description: "First Level Deutsch"}) ;
        moduleStore.put({id: 3, parentId: 1, classId: 1, name: "Rechnen", description: "Second Level Math"}) ;
        moduleStore.put({id: 4, parentId: 1, classId: 1, name: "Geometrie", description: "Second Level Math"}) ;
        moduleStore.put({id: 5, parentId: 2, classId: 1, name: "Lesen", description: "Second Level Deutsch"}) ;
        moduleStore.put({id: 6, parentId: 2, classId: 1, name: "Schreiben", description: "Second Level Deutsch"}) ;
        moduleStore.put({id: 7, parentId: 3, classId: 1, name: "Plus", description: "Third Level Math"}) ;
        moduleStore.put({id: 8, parentId: 3, classId: 1, name: "Minus", description: "Third Level Math"}) ;
        moduleStore.put({id: 9, parentId: 5, classId: 1, name: "Verstehen", description: "Third Level Math"}) ;
        moduleStore.put({id: 10, parentId: 5, classId: 1, name: "Wortgruppen", description: "Third Level Math"}) ;
        moduleStore.put({id: 11, parentId: 0, classId: 2, name: "Mathe", description: "First Level Math"}) ;
        moduleStore.put({id: 12, parentId: 0, classId: 2, name: "Deutsch", description: "First Level Deutsch"}) ;
        moduleStore.put({id: 13, parentId: 11, classId: 2, name: "Rechnen", description: "Second Level Math"}) ;
        moduleStore.put({id: 14, parentId: 11, classId: 2, name: "Geometrie", description: "Second Level Math"}) ;
        moduleStore.put({id: 15, parentId: 12, classId: 2, name: "Lesen", description: "Second Level Deutsch"}) ;
        moduleStore.put({id: 16, parentId: 12, classId: 2, name: "Schreiben", description: "Second Level Deutsch"}) ;
        moduleStore.put({id: 17, parentId: 13, classId: 2, name: "Plus", description: "Third Level Math"}) ;
        moduleStore.put({id: 18, parentId: 13, classId: 2, name: "Minus", description: "Third Level Math"}) ;
        moduleStore.put({id: 19, parentId: 15, classId: 2, name: "Verstehen", description: "Third Level Math"}) ;
        moduleStore.put({id: 20, parentId: 15, classId: 2, name: "Wortgruppen", description: "Third Level Math"}) ;
       
        var studentStore = db.createObjectStore('student', {keyPath: 'id', autoIncrement:true});
        studentStore.createIndex('name_idx', 'name', {unique: true});
        studentStore.put({id: 1, name: "Horti Klein", date: "2008-05-10"}) ;
        studentStore.put({id: 2, name: "Borsti Lanf", date: "2008-06-17"}) ;
        studentStore.put({id: 3, name: "Jana Lanf", date: "2007-06-17"}) ;
        
        var studentClassStore = db.createObjectStore('student_class', {keyPath: 'id', autoIncrement:true});
        studentClassStore.createIndex('studentId_idx', 'studentId', {unique: false});
        studentClassStore.createIndex('classId_idx', 'classId', {unique: false});
        studentClassStore.put({id: 1, studentId: 1, classId: 1}) ;
        studentClassStore.put({id: 2, studentId: 2, classId: 1}) ;
        studentClassStore.put({id: 3, studentId: 3, classId: 2}) ;
        
        var classUserStore = db.createObjectStore('class_user', {keyPath: 'id', autoIncrement:true});
        classUserStore.createIndex('classId_idx', 'classId', {unique: false});
        classUserStore.createIndex('userId_idx', 'userId', {unique: false});
        classUserStore.put({id: 1, classId: 1, userId: 2, type: 1}) ;
        classUserStore.put({id: 2, classId: 1, userId: 1, type: 2}) ;
        classUserStore.put({id: 3, classId: 2, userId: 1, type: 1}) ;
        
        var moduleUserStore = db.createObjectStore('module_user', {keyPath: 'id', autoIncrement:true});
        moduleUserStore.createIndex('moduleId_idx', 'moduleId', {unique: false});
        moduleUserStore.createIndex('userId_idx', 'userId', {unique: false});
        moduleUserStore.put({id: 1, moduleId: 1, userId: 1, type: 1}) ;
        moduleUserStore.put({id: 2, moduleId: 2, userId: 2, type: 1}) ;
        
        var ratingStore = db.createObjectStore('rating', {keyPath: 'id', autoIncrement:true});
        ratingStore.createIndex('userId_idx', 'userId', {unique: false});
        ratingStore.createIndex('studentId_idx', 'studentId', {unique: false});
        ratingStore.createIndex('moduleId_idx', 'classModuleId', {unique: false});
        ratingStore.put({id: 1, moduleId: 1, studentId: 1, userId: 1, value: 1, addInfo: "Gerade so"}) ;
        ratingStore.put({id: 2, moduleId: 2, studentId: 1, userId: 1, value: 4, addInfo: "Super"}) ;
        ratingStore.put({id: 3, moduleId: 3, studentId: 1, userId: 1, value: 3, addInfo: "Naja"}) ;
        ratingStore.put({id: 4, moduleId: 10, studentId: 1, userId: 1, value: 2, addInfo: "Ok"}) ;
        //console.log(new Date() + 'End: upgradeDatabaseTo1');
}


function go_it( $scope, bit){
    $scope.bitAllFromIndexDB=$scope.bitAllFromIndexDB||bit;
    //console.log($scope.bitAllFromIndexDB);
    if ($scope.bitAllFromIndexDB == 255){
        //console.log(JSON.stringify($scope.allObjectsIndexDB, null, 4));
    }
}
function getAllFromIndexDB( $indexedDB, $scope){
    $scope.bitAllFromIndexDB=0;
    $indexedDB.openStore('class', function ( store){ 
        
        store.each().then( function(modules){$scope.allObjectsIndexDB ["class"]=modules;go_it( $scope, 1 );});
    })
     
    $indexedDB.openStore('user', function ( store){ 
        store.each().then( function(modules){$scope.allObjectsIndexDB ["user"]=modules;go_it( $scope, 2);});
    })
     
    $indexedDB.openStore('module', function ( store){ 
        store.each().then( function(modules){$scope.allObjectsIndexDB ["module"]=modules;go_it( $scope, 4);});
    })
    
    $indexedDB.openStore('student', function ( store){ 
        store.each().then( function(modules){$scope.allObjectsIndexDB ["student"]=modules;go_it( $scope, 8);});
    })
      
    $indexedDB.openStore('student_class', function ( store){ 
        store.each().then( function(modules){$scope.allObjectsIndexDB ["student_class"]=modules;go_it( $scope, 16);});
    })
      
    $indexedDB.openStore('class_user', function ( store){ 
        store.each().then( function(modules){$scope.allObjectsIndexDB ["class_user"]=modules;go_it( $scope, 32);});
    })
      
    $indexedDB.openStore('module_user', function ( store){ 
        store.each().then( function(modules){$scope.allObjectsIndexDB ["module_user"]=modules;go_it( $scope, 64);});
    })
      
    $indexedDB.openStore('rating', function ( store){ 
        store.each().then( function(modules){$scope.allObjectsIndexDB["rating"]=modules;go_it( $scope, 128);});
    })
}

function getModuleInformation( $indexedDB, $scope){
    
    return $indexedDB.openStore('module', function ( store){
        ////console.log(new Date() + ' start getModuleHierarchy');
        ////console.log($scope.selectedClass);
        
        store.eachWhere(store.query().$eq($scope.selectedClass.id).$index('classId_idx')).then( function(modules){
            
        $scope.moduleInfo[0] = { id:0, name:"", description:"root", parentId:0};

            for (var i in modules) {
                var module = modules[i];
                
                if (module["deleted"] === undefined){
                    $scope.moduleInfo[module.id] = { id:module.id, name:module.name, description:module.description, parentId:module.parentId , weight:module.weight };
                    
                    if ($scope.moduleHierarchy[module.parentId] === undefined){
                        $scope.moduleHierarchy[module.parentId] = [module.id];
                    }else{
                        $scope.moduleHierarchy[module.parentId].push(module.id);
                    }
                }
            }
            ////console.log($scope.moduleInfo);
            ////console.log($scope.moduleHierarchy);
            ////console.log(new Date() + ' ende getModuleHierarchy');
        });
    });
}

function getRatings( $indexedDB, $scope){
    return $indexedDB.openStore('rating', function( store){
        ////console.log(new Date() + ' start getRatings');
        store.each().then(function(ratings) {
            ////console.log(new Date() + ' start getRatings');
            for (var i in ratings) {
                var rating = ratings[i];
                
                if (rating["deleted"] === undefined){
                    if ($scope.moduleRating[rating.moduleId] === undefined){
                        $scope.moduleRating[rating.moduleId] = {}
                    }
                    if ($scope.moduleRating[rating.moduleId][rating.studentId] === undefined){
                            $scope.moduleRating[rating.moduleId][rating.studentId] = {moduleId:rating.moduleId, value:rating.value, addInfo:rating.addInfo, studentId:rating.studentId, id:rating.id};
                    }else{
                        //console.log("Rating is not unique");
                        //console.log(rating);
                    }
                }
            }; 
            
            ////console.log($scope.moduleRating);
            ////console.log(new Date() + ' ende getRatings');
        });
    });    
}

function getUsers( $indexedDB, $scope){
    return $indexedDB.openStore('user', function( store){
        ////console.log(new Date() + ' start getRatings');
        store.each().then(function(users) {
            ////console.log(new Date() + ' start getRatings');
            $scope.dropDownUser = [];
            for (var i in users) {
                var user = users[i];
                if (user["deleted"] === undefined){
                    $scope.dropDownUser.push({ id: user.id, name: user.name});
                }
            }; 
        });
    });    
}

function getClasses( $indexedDB, $scope){
    return $indexedDB.openStore('class', function( store){
        $scope.dropDownClass = []
        store.each().then(function(classes) {
            for (var i in classes) {
                var myClass = classes[i];
                if (myClass["deleted"] === undefined){
                    $scope.dropDownClass.push({ id: myClass.id, name: myClass.name, level:myClass.level});
                }
            }; 
        });
    });    
}

function getStudents( $indexedDB, classId){

    return $indexedDB.openStore('student_class', function( store){
        return store.eachWhere(store.query().$eq(classId).$index('classId_idx')).then(function(studentClasses) {
            var studentIds = [];
            for (var i in studentClasses) {
                //console.log(i);
                var studentClass = studentClasses[i];
                //console.log(studentClass);
                if (studentClass["deleted"] === undefined){
                    studentIds.push( studentClass.studentId );
                }
            };
            
            return studentIds;
        }).then(function(studentIds) {
            return $indexedDB.openStore('student', function( store){
                return store.each().then(function(students) {
                    var retStudents = [];
                    for (var i in students) {
                        var student = students[i];
                        if ( inArray(studentIds,student.id)){
                            retStudents.push( { id: student.id, name: student.name, bdate: student.date});
                        }
                    }; 
                    return retStudents;
                });
            });
        });
    });
}

function saveRating( $indexedDB, $scope, rating){
    return $indexedDB.openStore('rating', function ( store){
        if (rating["id"] === undefined){
            delete rating["id"];
        }
        return store.upsert(rating);
    });
}

function saveModule( $indexedDB, $scope, module){

    return $indexedDB.openStore('module', function ( store){
        if (module["id"] === undefined){
            delete module["id"];
        }
        return store.upsert(module);
    });
}

function deleteModule( $indexedDB, $scope, moduleId){

    return $indexedDB.openStore('module', function ( store){
		return store.delete(moduleId);
	});
}

function saveModuleUser( $indexedDB, $scope, moduleUser){

    return $indexedDB.openStore('module_user', function ( store){
		if (moduleUser["id"] === undefined){
			delete moduleUser["id"];
		}
		return store.upsert(moduleUser);
	});
}


function saveClass( $indexedDB, $scope, myClass){

    return $indexedDB.openStore('class', function ( store){
        if (myClass["id"] === undefined){
            delete myClass["id"];
        }
		//console.log(myClass);
        return store.upsert(myClass);
    });
}

function deleteClass( $indexedDB, $scope, classId){

    return $indexedDB.openStore('class', function ( store){
		return store.delete(classId);
	});
}


function saveStudentClass( $indexedDB, $scope, oStudentClass){

	var dbStudentClass = {};
	dbStudentClass["classId"] = oStudentClass["classId"];
	dbStudentClass["studentId"] = oStudentClass["id"];
	
    return $indexedDB.openStore('student_class', function ( store){
		return store.upsert(dbStudentClass);
	});
}

function saveStudent( $indexedDB, $scope, oStudent){
	
	var dbStudent = {};
	
	if (oStudent["id"] !== undefined){
        dbStudent["id"] = oStudent["id"];
    }
	dbStudent["name"] = oStudent["name"];
	dbStudent["date"] = oStudent["bdate"];
	
    return $indexedDB.openStore('student', function ( store){
        return store.upsert(dbStudent);
    }).then( function(id) {
		if (id != oStudent["id"]){
			//console.log("oStudent");
			//console.log(oStudent);
			//console.log(id);
			oStudent["id"] = id[0];
			return saveStudentClass( $indexedDB, $scope, oStudent)
		}
	});
}

function deleteStudent( $indexedDB, $scope, oStudent){

    return $indexedDB.openStore('student', function ( store){
		return store.delete(classId);
	});
}