angular.module('ui.bootstrap.demo', ['ui.bootstrap', 'indexedDB']).config(function ($indexedDBProvider) {
    $indexedDBProvider
      .connection('schoolIdDBv06')
      .upgradeDatabase(1, upgradeDatabaseTo1);
 });

function getRating($scope, moduleId, studentId) {
    
    if ($scope.moduleRating[moduleId] === undefined) {
        return { moduleId: moduleId, value: '-', addInfo: '', studentId: studentId, id: undefined };
    }
    if ($scope.moduleRating[moduleId][studentId] === undefined) {
        return { moduleId: moduleId, value: '-', addInfo: '', studentId: studentId, id: undefined };
    }
    
    return $scope.moduleRating[moduleId][studentId];
}

function getRowArray($scope, module, i){
    
	i = i + 1;
    var item = {};
    var data = [];
	
    var rating = getRating($scope, module, $scope.selectedStudent.id);
    //console.log($scope.moduleInfo[module]['name']);
	
    item['name'] = $scope.moduleInfo[module]['name'];
    item['description'] = $scope.moduleInfo[module]['description'];
    item['moduleId'] = rating.moduleId;
    item['studentId'] = rating.studentId;
    item['addInfo'] = rating.addInfo;
    item['rating'] = rating.value;
    item['ratingId'] = rating.id;
	
    for (var i in $scope.moduleHierarchy[module]) {
        
        if ($scope.moduleHierarchy[module][i] == module ){
            continue;
        }
		
        data.push(getRowArray($scope, $scope.moduleHierarchy[module][i], i));
    }
    
    item['data'] = data;
    
    return item;
}

angular.module('ui.bootstrap.demo').controller('ModalDemoCtrl', function ($scope,  $indexedDB, $modal, $q) {
    
    $scope.childItem = 0;
    
    $scope.dropDownClass = [];
    //$scope.selectedClass = {id:1, name:"Dachs"};
    $scope.selectedClass = undefined;
    
    $scope.dropDownStudent = [];
    //$scope.selectedStudent = {id:1, name:"Hans"};
    $scope.selectedStudent = undefined;
    
    $scope.dropDownUser = [];
    //$scope.selectedUser = {id:1, name:"Frank"};
    $scope.selectedUser = undefined;
    
    $scope.state = 0;
    $scope.counter = 0;
    
    //$scope.allObjectsIndexDB = {};
    //getAllFromIndexDB( $indexedDB, $scope); return;
    
    $scope.buildMenu = function(){
        var userPromise = getUsers( $indexedDB, $scope).then( function(){ 
            var existsSelectedUser = false;
            for (var k in $scope.dropDownUser) {
                if ( $scope.selectedUser != undefined && $scope.selectedUser.id == $scope.dropDownUser[k].id){
                    existsSelectedUser = true;
                    //console.log("existsSelectedUser");
                }
            }
            if (existsSelectedUser === false){
                //console.log("set selectedUser");
                $scope.selectedUser = $scope.dropDownUser[0];
            }
			
         });
         
         var classStudentsPromise = getClasses( $indexedDB, $scope).then( function(){ 
                var existsSelectedClass = false;
				
                for (var k in $scope.dropDownClass) {
                    if ( $scope.selectedClass != undefined && $scope.selectedClass.id == $scope.dropDownClass[k].id){
                        existsSelectedClass = true;
                        //console.log("existsSelectedClass");
                    }
                }
                if (existsSelectedClass === false){
                    //console.log("set selectedClass");
                    $scope.selectedClass = $scope.dropDownClass[0];
                }
          }).then( function( ){  

                    return getStudents( $indexedDB, $scope.selectedClass.id).then( function( students){ 
                        var existsSelectedStudent = false;
                        $scope.dropDownStudent = students;
						
                        for (var k in $scope.dropDownStudent) {
                            if ( $scope.selectedStudent != undefined && $scope.selectedStudent.id == $scope.dropDownStudent[k].id){
                                existsSelectedStudent = true;
                            }
                        }
                        if (existsSelectedStudent === false){
                            $scope.selectedStudent = $scope.dropDownStudent[0];
                        }
                    });    
					
        });
        
        $q.all([ classStudentsPromise, userPromise ]).then(function(responsesArray) {
            $scope.buildStoreData();
        });
    }
    
    $scope.buildStoreData = function(){
        //console.log(new Date() + ' start buildStoreData');
        
        $scope.moduleHierarchy = {};
        $scope.moduleRating = {};
        $scope.moduleInfo = {};
    
        var moduleInfoPromise = getModuleInformation( $indexedDB, $scope);
        var ratingPromise = getRatings( $indexedDB, $scope);
       
        
        $q.all([ moduleInfoPromise, ratingPromise ]).then(function(responsesArray) {
            $scope.$evalAsync(function() {
                $scope.storedata.data = [];
				//console.log("$scope.moduleHierarchy");
				//console.log($scope.moduleHierarchy);
				//console.log("root");
				//console.log($scope.moduleHierarchy[0]);
    
                for (var k in $scope.moduleHierarchy[0]) {
					//console.log(k);
					//console.log($scope.moduleHierarchy[0][k]);
                    //for ( i in $scope.moduleHierarchy[0][k]){	
					//console.log('******************'); 
					 
					data = getRowArray($scope, $scope.moduleHierarchy[0][k], 0);
					
					$scope.storedata.data.push(data);
                    //}
                }
                //console.log(new Date() + ' end buildStoreData');
            });
        });
    }
    console.log("D1");
    $scope.storedata = { data: []};
	
    $scope.buildMenu ();
    
    
    $scope.setRating = function(data, row, value, addInfo) {
        for (var k in data) {
            var item = data[k];
            if (item.moduleId == row.moduleId && item.studentId == row.studentId){
                item.rating = value;
                item.addInfo = addInfo;
                return;
            }
            
            $scope.setRating(item.data, row, value, addInfo);
        }
    }
    
    $scope.setRatingId = function(data, row, id) {
    
        for (var k in data) {
            var item = data[k];
            if (item.moduleId == row.moduleId && item.studentId == row.studentId){
                item.ratingId = id;
                return;
            }
            
            $scope.setRatingId(item.data, row, id);
        }
        
    }
    
    $scope.changeRating = function(row, value, addInfo) {
    
        $scope.setRating($scope.storedata.data, row, value, addInfo);
        saveRating( $indexedDB, $scope, { 'moduleId': row.moduleId
                                        , 'studentId': row.studentId
                                        , 'userId': 1, 'value': value
                                        , 'addInfo': addInfo
                                        , 'id': row.ratingId }).then(function(response) {
            $scope.setRatingId($scope.storedata.data, row, response[0]);
        });
    }
    
    $scope.changeRatingValue = function(row, value) {
        $scope.changeRating(row, value, row.addInfo);
    }
    
    $scope.changeRatingAddInfo = function(row, addInfo) {
        $scope.changeRating(row, row.rating, addInfo);
    }
     
    $scope.setDropDownClass = function(myClass) {
        $scope.selectedClass = myClass;
        $scope.buildMenu();
    } 
    
    $scope.setDropDownStudent = function(student) {
        $scope.selectedStudent = student;
        $scope.buildMenu();
    } 
    
    $scope.setDropDownUser = function(myUser) {
        $scope.selectedUser = myUser;
        $scope.buildMenu();
    } 
    
    $scope.openModule = function (p_moduleId, p_module) {
        console.log('p_module');
        console.log(p_moduleId);
        console.log(p_module);
        
        var modalInstance = $modal.open({
            templateUrl: 'modalModul.html',
            controller: 'ModalInstanceCtrl',
            size: '',
            resolve: {
                info: function () {
                    var info = {};
                    var module = $scope.moduleInfo[p_moduleId];

                    var childs = 1;
                    if ($scope.moduleHierarchy[p_moduleId] === undefined || 
                            $scope.moduleHierarchy[p_moduleId].length === 0 ) {
                        childs = 0;
                    }
                    
                    if ( p_module === undefined ){
                        info = {  moduleName: ""
                                , moduleWeight: 1
                                , moduleClass: $scope.selectedClass.name
                                , parentId: module.id
                                , parentName: module.name
                                , childs: childs 
                                , newModule: 1
                                };
                    }else{
                        var parentId = $scope.moduleInfo[p_moduleId].parentId;
                        var parentModule = $scope.moduleInfo[parentId];

                        info = { moduleId: module.id
                                , moduleName: module.name
                                , moduleWeight: module.weight
                                , moduleClass: $scope.selectedClass.name
                                , parentId: parentModule.id
                                , parentName: parentModule.name
                                , childs: childs 
                                , newModule: 0
                                };
                    }
                    
                    return info;
                }
            }
        });
        
        modalInstance.result.then(function (info) {
            //console.log('result');
            console.log(info);
            
            if (info.deleted  === undefined || info.deleted == 0) { 
                if (info.moduleId == 0 || info.moduleId == undefined){
                    moduleId = undefined;
                }else{
                        moduleId = info.moduleId;
                }
                    
                module = {  id: moduleId, 
                            parentId: info.parentId, 
                            classId: $scope.selectedClass.id, 
                            name: info.moduleName, 
                            description: "",
                            weight: info.moduleWeight
                         };
                    
                saveModule( $indexedDB, $scope, module).then( function(id) {
                    $scope.buildStoreData();
                });
            } else {
                deleteModule( $indexedDB, $scope, info.moduleId).then( function(id) {
                    $scope.buildStoreData();
                });
            }
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
    };
	
    $scope.openClass = function (p_class) {
        //console.log("hier");
        var modalInstance = $modal.open({
            templateUrl: 'modalClass.html',
            controller: 'ModalClassInstanceCtrl',
            resolve: {
                info: function () {
                    var info = {};
                  
					if (p_class === undefined){
                        info = {};
                    }else {
						info = p_class; 
					}
					
					//console.log('input');
					//console.log(info);
                    return info;
                }
            }
        });
        
        modalInstance.result.then(function (info) {
            //console.log('result');
            //console.log(info);
            
            if (info.deleted  === undefined || info.deleted == 0) { 
                
                myClass = { id: info.id, 
                            name: info.name, 
                            level: info.level };
                    
                saveClass( $indexedDB, $scope, myClass).then( function(id) {
                $scope.buildMenu();
            });
            } else {
                deleteClass( $indexedDB, $scope, info.id).then( function(id) {
                    $scope.buildMenu();
                });
            }
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
    };
	
	$scope.openStudent = function (p_class, p_student) {
        
        var modalInstance = $modal.open({
            templateUrl: 'modalStudent.html',
            controller: 'ModalStudentInstanceCtrl',
            resolve: {
                info: function () {
                    var info = {};
					
					//console.log("p_class");
					//console.log(p_class);
					//console.log("p_student");
					//console.log(p_student);
					
					info['classId'] = p_class.id;
					info['className'] = p_class.name;
					
					if (p_student !== undefined){
						info['id'] = p_student.id; 
						info['name'] = p_student.name; 
						info['bdate'] = p_student.bdate; 
					}
					
					//console.log('input');
					//console.log(info);
                    return info;
                }
            }
        });
        
        modalInstance.result.then(function (info) {
            //console.log('result');
            //console.log(info);
            
            if (info.deleted  === undefined || info.deleted == 0) { 
                
                oStudent = { id: info.id, 
							classId: info.classId, 
                            name: info.name, 
                            bdate: info.bdate };
                    
                saveStudent( $indexedDB, $scope, oStudent).then( function(id) {
                $scope.buildMenu();
            });
            } else {
                deleteStudent( $indexedDB, $scope, info.id).then( function(id) {
                    $scope.buildMenu();
                });
            }
        }, function () {
            //console.log('Modal Student dismissed at: ' + new Date());
        });
    };
});

angular.module('ui.bootstrap.demo').controller('ModalInstanceCtrl', function ($scope, $modalInstance, info) {
      
    $scope.info = info;
    
    if($scope.info.newModule == 1){
        $scope.title = "Hinzufügen eines Moduls";
        $scope.func = "Hinzufügen"
    }else{
        $scope.title = "Bearbeiten eines Moduls";
        $scope.func = "Speichern"
    }
    //console.log( info);

    $scope.ok = function () {
        $modalInstance.close($scope.info);
    };

    $scope.delete = function () {
        
        $scope.info['deleted'] = 1;
        $modalInstance.close($scope.info);
    };

    
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


angular.module('ui.bootstrap.demo').controller('ModalClassInstanceCtrl', function ($scope, $modalInstance, info) {
      
    $scope.info = info;
    
    if($scope.info.newModule == 1){
        $scope.title = "Hinzufügen einer Klasse";
        $scope.func = "Hinzufügen"
    }else{
        $scope.title = "Bearbeiten einer Klasse";
        $scope.func = "Speichern"
    }
    //console.log( info);

    $scope.ok = function () {
        $modalInstance.close($scope.info);
    };

    $scope.delete = function () {
        
        $scope.info['deleted'] = 1;
        $modalInstance.close($scope.info);
    };

    
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

angular.module('ui.bootstrap.demo').controller('ModalStudentInstanceCtrl', function ($scope, $modalInstance, info) {
      
    $scope.info = info;
    
    if($scope.info.newModule == 1){
        $scope.title = "Hinzufügen eines Schülers";
        $scope.func = "Hinzufügen"
    }else{
        $scope.title = "Bearbeiten eines Schülers";
        $scope.func = "Speichern"
    }
    //console.log( info);

    $scope.ok = function () {
        $modalInstance.close($scope.info);
    };

    $scope.delete = function () {
        
        $scope.info['deleted'] = 1;
        $modalInstance.close($scope.info);
    };

    
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
